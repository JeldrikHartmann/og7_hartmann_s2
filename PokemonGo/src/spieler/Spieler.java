package spieler;

import java.util.ArrayList;
import java.util.Arrays;

import pokemon.Pokemon;

public class Spieler {

	private String name;
	private int sternenstaub;
	private Pokemon[] team = new Pokemon[6];
	private ArrayList<Pokemon> inBesitz;
	private ArrayList<Bonbon> bonbons;
	public Spieler() {
		super();
	}
	public Spieler(String name, int sternenstaub, Pokemon[] team, ArrayList<Pokemon> inBesitz,
			ArrayList<Bonbon> bonbons) {
		super();
		this.name = name;
		this.sternenstaub = sternenstaub;
		this.team = team;
		this.inBesitz = inBesitz;
		this.bonbons = bonbons;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSternenstaub() {
		return sternenstaub;
	}
	public void setSternenstaub(int sternenstaub) {
		this.sternenstaub = sternenstaub;
	}
	public Pokemon[] getTeam() {
		return team;
	}
	public void setTeam(Pokemon[] team) {
		this.team = team;
	}
	public ArrayList<Pokemon> getInBesitz() {
		return inBesitz;
	}
	public void setInBesitz(ArrayList<Pokemon> inBesitz) {
		this.inBesitz = inBesitz;
	}
	public ArrayList<Bonbon> getBonbons() {
		return bonbons;
	}
	public void setBonbons(ArrayList<Bonbon> bonbons) {
		this.bonbons = bonbons;
	}
	public void f�geZuTeamHinzu(int indexInBesitz, int indexTeam) {
		team[indexTeam] = inBesitz.get(indexInBesitz);
	}
	public void l�scheAusTeam(int indexTeam) {
		team[indexTeam] = null;
	}
	public void powerUp(int indexInBesitz) {
		
	}

	public void verschicken(int indexInBesitz) {
		inBesitz.remove(indexInBesitz);
	}
	@Override
	public String toString() {
		return "Spieler [name=" + name + ", sternenstaub=" + sternenstaub + ", team=" + Arrays.toString(team)
				+ ", inBesitz=" + inBesitz + ", Bonbons=" + bonbons + "]";
	}
	public Bonbon findeBonbon(int index) {
		for (Bonbon b : bonbons) {
			if(b.getPokemonArt().getArtName().equals(this.getInBesitz().get(index).getPokemonArt().getArtName())) {
				return b;
			}
			
		}
		return null;
	}
}
