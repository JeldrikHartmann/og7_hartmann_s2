package spieler;

public class Kampf {

	private Spieler spieler1;
	private Spieler spieler2;
	public Kampf() {
		super();
	}
	public Kampf(Spieler spieler1, Spieler spieler2) {
		super();
		this.spieler1 = spieler1;
		this.spieler2 = spieler2;
	}
	public Spieler getSpieler1() {
		return spieler1;
	}
	public void setSpieler1(Spieler spieler1) {
		this.spieler1 = spieler1;
	}
	public Spieler getSpieler2() {
		return spieler2;
	}
	public void setSpieler2(Spieler spieler2) {
		this.spieler2 = spieler2;
	}
	@Override
	public String toString() {
		return "Kampf [spieler1=" + spieler1 + ", spieler2=" + spieler2 + "]";
	}
}
