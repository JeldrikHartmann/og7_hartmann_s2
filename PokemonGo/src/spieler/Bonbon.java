package spieler;

import pokemon.PokemonArt;

public class Bonbon {

	private int anzahl;
	private pokemon.PokemonArt pokemonArt;
	public Bonbon() {
		super();
	}
	public Bonbon(int anzahl, PokemonArt pokemonArt) {
		super();
		this.anzahl = anzahl;
		this.pokemonArt = pokemonArt;
	}
	public int getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	public pokemon.PokemonArt getPokemonArt() {
		return pokemonArt;
	}
	public void setPokemonArt(pokemon.PokemonArt pokemonArt) {
		this.pokemonArt = pokemonArt;
	}
	@Override
	public String toString() {
		return "Bonbon [anzahl=" + anzahl + ", pokemonArt=" + pokemonArt + "]";
	}
}
