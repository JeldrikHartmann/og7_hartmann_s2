package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import spieler.Spieler;

public class PokemonAnsicht extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7692406722375354199L;

	/**
	 * Create the panel.
	 * 
	 * @throws IOException
	 */
	public PokemonAnsicht(Spieler s, int index, CardLayout cl, Container contentPane) throws IOException {

		// Fenster Erstellen
		setBounds(0, 0, 400, 800);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);

		// Elemente erstellen
		if (!s.getInBesitz().get(index).isFavorit()) {
			JButton btnFavorit = new JButton(
					new ImageIcon(ImageIO.read(getClass().getResource("/objekte/FavoritenFalse.png"))));

			btnFavorit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!s.getInBesitz().get(index).isFavorit()) {
						try {
							btnFavorit.setIcon(
									new ImageIcon(ImageIO.read(getClass().getResource("/objekte/FavoritenTrue.png"))));
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						s.getInBesitz().get(index).setFavorit(true);
					} else {
						try {
							btnFavorit.setIcon(
									new ImageIcon(ImageIO.read(getClass().getResource("/objekte/FavoritenFalse.png"))));
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						s.getInBesitz().get(index).setFavorit(false);
					}
				}
			});

			btnFavorit.setBorder(BorderFactory.createEmptyBorder());
			btnFavorit.setOpaque(false);
			btnFavorit.setContentAreaFilled(false);
			btnFavorit.setBounds(345, 23, btnFavorit.getPreferredSize().width, btnFavorit.getPreferredSize().height);
			add(btnFavorit);
		} else {
			JButton btnFavorit = new JButton(
					new ImageIcon(ImageIO.read(getClass().getResource("/objekte/FavoritenTrue.png"))));

			btnFavorit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!s.getInBesitz().get(index).isFavorit()) {
						try {
							btnFavorit.setIcon(
									new ImageIcon(ImageIO.read(getClass().getResource("/objekte/FavoritenTrue.png"))));
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						s.getInBesitz().get(index).setFavorit(true);
					} else {
						try {
							btnFavorit.setIcon(
									new ImageIcon(ImageIO.read(getClass().getResource("/objekte/FavoritenFalse.png"))));
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						s.getInBesitz().get(index).setFavorit(false);
					}
				}
			});

			btnFavorit.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
			btnFavorit.setOpaque(false);
			btnFavorit.setContentAreaFilled(false);
			btnFavorit.setBounds(345, 23, btnFavorit.getPreferredSize().width, btnFavorit.getPreferredSize().height);
			add(btnFavorit);
		}

		// Head
		JLabel lblWp = new JLabel(Integer.toString(s.getInBesitz().get(index).getWp()));
		JLabel lblWpTitel = new JLabel("WP");
		JLabel lblPName = new JLabel(s.getInBesitz().get(index).getName());
		JLabel lblPbild = new JLabel(new ImageIcon(s.getInBesitz().get(index).getPokemonArt().getBild()));
		lblWp.setFont(new Font("SansSerif", Font.PLAIN, 25));
		lblWp.setForeground(Color.WHITE);
		lblWpTitel.setFont(new Font("SansSerif", Font.PLAIN, 15));
		lblWpTitel.setForeground(Color.WHITE);
		lblWpTitel.setBounds(mitte(lblWp, lblWpTitel), 25, lblWpTitel.getPreferredSize().width,
				lblWpTitel.getPreferredSize().height);
		lblWp.setBounds(mitte(lblWp, lblWpTitel) + lblWpTitel.getPreferredSize().width, 15,
				lblWp.getPreferredSize().width, lblWp.getPreferredSize().height);
		lblPName.setFont(new Font("SansSerif", Font.PLAIN, 25));
		lblPName.setForeground(Color.decode("#5280e9"));
		lblPName.setBounds(mitte(lblPName), 244, lblPName.getPreferredSize().width, lblPName.getPreferredSize().height);
		lblPbild.setBounds(mitte(lblPbild), 10000 / lblPbild.getPreferredSize().height,
				lblPbild.getPreferredSize().width, lblPbild.getPreferredSize().height);
		add(lblWpTitel);
		add(lblWp);
		add(lblPName);
		add(lblPbild);

		// KP
		JProgressBar KpBar = new JProgressBar();
		JLabel lblKp = new JLabel(
				"KP " + s.getInBesitz().get(index).getAktKp() + " / " + s.getInBesitz().get(index).getMaxKp());
		KpBar.setForeground(Color.GREEN);
		KpBar.setValue(s.getInBesitz().get(index).getAktKp() * 100 / s.getInBesitz().get(index).getMaxKp());
		KpBar.setBounds(110, 280, 180, 5);
		if (KpBar.getValue() < 50) {
			KpBar.setForeground(Color.YELLOW);
			if (KpBar.getValue() < 25) {
				KpBar.setForeground(Color.RED);
			}
		}
		lblKp.setBounds(mitte(lblKp), 290, lblKp.getPreferredSize().width, lblKp.getPreferredSize().height);
		add(KpBar);
		add(lblKp);

		// Daten
		JLabel lblTyp = new JLabel(s.getInBesitz().get(index).getPokemonArt().getTyp());
		JLabel lblTypTitel = new JLabel("Typ");
		JLabel lblGewicht = new JLabel(s.getInBesitz().get(index).getGewicht() + "kg");
		JLabel lblGewichtTitel = new JLabel("Gewicht");
		JLabel lblGröße = new JLabel(s.getInBesitz().get(index).getGröße() + "m");
		JLabel lblGrößeTitel = new JLabel("Größe");
		lblTyp.setBounds(mitte(lblTyp) - 110, 330, lblTyp.getPreferredSize().width * 2,
				lblTyp.getPreferredSize().height + 2);
		lblTyp.setFont(new Font("SansSerif", Font.PLAIN, 15));
		lblTyp.setForeground(Color.decode("#5280e9"));
		lblTypTitel.setBounds(mitte(lblTypTitel) - 105, 350, lblTyp.getPreferredSize().width,
				lblTyp.getPreferredSize().height);
		lblTypTitel.setFont(new Font("SansSerif", Font.PLAIN, 10));
		lblTypTitel.setForeground(Color.GRAY);
		lblGewicht.setBounds(mitte(lblGewicht) + 20, 330, lblGewicht.getPreferredSize().width + 10,
				lblGewicht.getPreferredSize().height + 2);
		lblGewicht.setFont(new Font("SansSerif", Font.PLAIN, 15));
		lblGewicht.setForeground(Color.decode("#5280e9"));
		lblGewichtTitel.setBounds(mitte(lblGewichtTitel) + 25, 350, lblGewichtTitel.getPreferredSize().width,
				lblGewichtTitel.getPreferredSize().height);
		lblGewichtTitel.setFont(new Font("SansSerif", Font.PLAIN, 10));
		lblGewichtTitel.setForeground(Color.GRAY);
		lblGröße.setBounds(mitte(lblGröße) + 100, 330, lblGröße.getPreferredSize().width + 10,
				lblGröße.getPreferredSize().height + 2);
		lblGröße.setFont(new Font("SansSerif", Font.PLAIN, 15));
		lblGröße.setForeground(Color.decode("#5280e9"));
		lblGrößeTitel.setBounds(mitte(lblGrößeTitel) + 105, 350, lblGrößeTitel.getPreferredSize().width,
				lblGrößeTitel.getPreferredSize().height);
		lblGrößeTitel.setFont(new Font("SansSerif", Font.PLAIN, 10));
		lblGrößeTitel.setForeground(Color.GRAY);
		add(lblTyp);
		add(lblTypTitel);
		add(lblGewicht);
		add(lblGewichtTitel);
		add(lblGröße);
		add(lblGrößeTitel);

		// Sternenstaub
		JLabel lblSternenstaub = new JLabel(Integer.toString(s.getSternenstaub()));
		JLabel lblSternenstaubBild = new JLabel();
		JLabel lblSternenstaubTitel = new JLabel("STERNENSTAUB");
		lblSternenstaub.setBounds(mitte(lblSternenstaub) - 80, 400, lblSternenstaub.getPreferredSize().width * 2,
				lblSternenstaub.getPreferredSize().height);
		lblSternenstaub.setFont(new Font("SansSerif", Font.PLAIN, 15));
		lblSternenstaub.setForeground(Color.decode("#5280e9"));
		add(lblSternenstaub);
		lblSternenstaubBild.setIcon(new ImageIcon(ImageIO.read(getClass().getResource("/icons/Sternenstaub.png"))));
		lblSternenstaubBild.setBounds(mitte(lblSternenstaub) - 95, 398, lblSternenstaubBild.getPreferredSize().width,
				lblSternenstaubBild.getPreferredSize().height);
		add(lblSternenstaubBild);
		lblSternenstaubTitel.setBounds(mitte(lblSternenstaubTitel) - 85, 420,
				lblSternenstaubTitel.getPreferredSize().width * 2, lblSternenstaubTitel.getPreferredSize().height);
		lblSternenstaubTitel.setFont(new Font("SansSerif", Font.PLAIN, 10));
		lblSternenstaubTitel.setForeground(Color.GRAY);
		add(lblSternenstaubTitel);

		// Bonbon
		JLabel lblBonbon = new JLabel(Integer.toString(s.findeBonbon(index).getAnzahl()));
		JLabel lblBonbonBild = new JLabel();
		JLabel lblBonbonTitel = new JLabel(
				s.getInBesitz().get(index).getPokemonArt().getArtName().toUpperCase() + "-BONBON");
		lblBonbon.setBounds(mitte(lblBonbon) + 70, 400, lblBonbon.getPreferredSize().width * 2,
				lblBonbon.getPreferredSize().height);
		lblBonbon.setFont(new Font("SansSerif", Font.PLAIN, 15));
		lblBonbon.setForeground(Color.decode("#5280e9"));
		lblBonbonBild.setIcon(new ImageIcon(ImageIO.read(getClass().getResource("/icons/Bonbon.png"))));
		lblBonbonBild.setBounds(mitte(lblBonbon) + 50, 398, lblBonbonBild.getPreferredSize().width,
				lblBonbonBild.getPreferredSize().height);
		lblBonbonTitel.setBounds(mitte(lblBonbonTitel) + 80, 420, lblBonbonTitel.getPreferredSize().width * 2,
				lblBonbonTitel.getPreferredSize().height);
		lblBonbonTitel.setFont(new Font("SansSerif", Font.PLAIN, 10));
		lblBonbonTitel.setForeground(Color.GRAY);
		add(lblBonbon);
		add(lblBonbonBild);
		add(lblBonbonTitel);

		// Exit
		JButton btnExit = new JButton(new ImageIcon(ImageIO.read(getClass().getResource("/objekte/Exit.png"))));
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cl.show(contentPane, "start");
				contentPane.remove(contentPane.getComponent(1));
			}
		});
		btnExit.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		btnExit.setOpaque(false);
		btnExit.setContentAreaFilled(false);
		btnExit.setBounds(mitte(btnExit), 700, btnExit.getPreferredSize().width, btnExit.getPreferredSize().height);
		add(btnExit);

		// Hintergrund
		JLabel lblFläche = new JLabel();
		JLabel lblHintergrund = new JLabel(s.getInBesitz().get(index).getPokemonArt().bekommeHintergrund());
		JLabel lblHintergrund2 = new JLabel(s.getInBesitz().get(index).getPokemonArt().bekommeHintergrund());
		lblHintergrund.setBounds(0, 0, 400, 400);
		lblFläche.setIcon(new ImageIcon(ImageIO.read(getClass().getResource("/objekte/Fläche.png"))));
		lblFläche.setBounds(10, 160, 380, 690);
		lblHintergrund2.setBounds(0, 400, 400, 400);
		add(lblFläche);
		add(lblHintergrund);
		add(lblHintergrund2);

	}

	public int mitte(Component c) {
		int pos = 200 - (c.getPreferredSize().width / 2);
		return pos;
	}

	public int mitte(Component c, Component c2) {
		int pos = 200 - ((c.getPreferredSize().width + c2.getPreferredSize().width) / 2);
		return pos;
	}

}
