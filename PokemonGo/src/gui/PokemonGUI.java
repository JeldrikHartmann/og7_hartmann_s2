package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import pokemon.Attacke;
import pokemon.Pokemon;
import pokemon.PokemonArt;
import pokemon.Spezialattacke;
import spieler.Bonbon;
import spieler.Spieler;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import javax.swing.UIManager;
import java.awt.SystemColor;

public class PokemonGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2551609304499412620L;
	private JPanel contentPane;
	private int i = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) throws IOException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					PokemonGUI frame = new PokemonGUI();
					frame.setResizable(false);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 */
	public PokemonGUI() throws IOException {

		// Attacken erstellen
		Attacke a1 = new Attacke("Stahlfl�gel", "Stahl", 15);
		Attacke a2 = new Attacke("Donnerschock", "Elektro", 5);
		Attacke a3 = new Attacke("Sondersensor", "Psycho", 12);
		Attacke a4 = new Attacke("Feuerwirbel", "Feuer", 14);
		Attacke a5 = new Attacke("Zen-Kopfsto�", "Psycho", 12);
		Attacke a6 = new Attacke("Standpauke", "Unlicht", 12);
		Attacke a7 = new Attacke("Erstauner", "Geist", 8);
		Attacke a8 = new Attacke("Eisenschweif", "Stahl", 15);
		Attacke a9 = new Attacke("Eisodem", "Eis", 10);
		Attacke a10 = new Attacke("Feuerzahn", "Feuer", 11);
		Attacke a11 = new Attacke("Aquaknarre", "Wasser", 5);
		Attacke a12 = new Attacke("Konter", "Kampf", 12);
		Attacke a13 = new Attacke("Dunkelklaue", "Geist", 9);
		Attacke a14 = new Attacke("Feuerodem", "Drache", 6);
		Attacke a15 = new Attacke("K�ferbiss", "K�fer", 5);

		// Spezialattacken erstellen
		Spezialattacke sa1 = new Spezialattacke("Hyperstrahl", "Normal", 70, 3);
		Spezialattacke sa2 = new Spezialattacke("Donner", "Elektro", 100, 4);
		Spezialattacke sa3 = new Spezialattacke("Samenbomben", "Pflanze", 55, 2);
		Spezialattacke sa4 = new Spezialattacke("Flammenwurf", "Feuer", 70, 3);
		Spezialattacke sa5 = new Spezialattacke("Bodyslam", "Normal", 50, 2);
		Spezialattacke sa6 = new Spezialattacke("Nahkampf", "Kampf", 100, 4);
		Spezialattacke sa7 = new Spezialattacke("Nachtnebel", "Geist", 60, 3);
		Spezialattacke sa8 = new Spezialattacke("Rammboss", "Stahl", 70, 3);
		Spezialattacke sa9 = new Spezialattacke("Psychoschock", "Psycho", 65, 3);
		Spezialattacke sa10 = new Spezialattacke("Feuersturm", "Feuer", 140, 5);
		Spezialattacke sa11 = new Spezialattacke("Psystrahl", "Psycho", 70, 3);
		Spezialattacke sa12 = new Spezialattacke("Nassschweif", "Wasser", 50, 2);
		Spezialattacke sa13 = new Spezialattacke("Knuddler", "Fee", 90, 4);
		Spezialattacke sa14 = new Spezialattacke("Schaufler", "Boden", 100, 4);
		Spezialattacke sa15 = new Spezialattacke("Drachenpuls", "Drache", 90, 4);

		// Pokemonarten erstellen
		PokemonArt pa1 = new PokemonArt("Aerodactyl", "Gestein / Flug");
		PokemonArt pa2 = new PokemonArt("Raikou", "Elektro");
		PokemonArt pa3 = new PokemonArt("Kokowei", "Pflanze / Psycho");
		PokemonArt pa4 = new PokemonArt("Flamara", "Feuer");
		PokemonArt pa5 = new PokemonArt("Hundemon", "Unlicht / Feuer");
		PokemonArt pa6 = new PokemonArt("Ursaring", "Normal");
		PokemonArt pa7 = new PokemonArt("Sengo", "Normal");
		PokemonArt pa8 = new PokemonArt("Dragonir", "Drache");
		PokemonArt pa9 = new PokemonArt("Pinsir", "K�fer");
		PokemonArt pa10 = new PokemonArt("Rossana", "Eis / Psycho");
		PokemonArt pa11 = new PokemonArt("Stollrak", "Stahl / Gestein");
		PokemonArt pa12 = new PokemonArt("Aquana", "Wasser");
		PokemonArt pa13 = new PokemonArt("Nebulak", "Geist / Gift");
		PokemonArt pa14 = new PokemonArt("Relaxo", "Normal");
		PokemonArt pa15 = new PokemonArt("Granbull", "Fee");
		PokemonArt pa16 = new PokemonArt("Pantimos", "Psycho / Fee");

		// Pokemon erstellen
		Pokemon p1 = new Pokemon(pa14, a5, sa5, "Relaxo", "29.07.2018", 182, 20, 1418, 453.17, 2.00, true);
		Pokemon p2 = new Pokemon(pa1, a1, sa1, "Aerodactyl", "26.07.2016", 52, 52, 311, 79.05, 2.03, false);
		Pokemon p3 = new Pokemon(pa15, a6, sa6, "Granbull", "21.08.2018", 125, 125, 1246, 46.25, 1.44, true);
		Pokemon p4 = new Pokemon(pa2, a2, sa2, "Raikou", "21.08.2018", 119, 119, 1601, 213.21, 2.06, false);
		Pokemon p5 = new Pokemon(pa13, a7, sa7, "Nebulak", "12.08.2018", 69, 69, 637, 0.09, 1.23, false);
		Pokemon p6 = new Pokemon(pa3, a3, sa3, "Kokowei", "09.05.2018", 159, 159, 2295, 143.41, 1.93, true);
		Pokemon p7 = new Pokemon(pa11, a8, sa8, "Stollrak", "30.07.2018", 116, 116, 1611, 149.77, 0.92, false);
		Pokemon p8 = new Pokemon(pa4, a4, sa4, "Flamara", "30.07.2018", 118, 118, 2257, 37.92, 1.05, true);
		Pokemon p9 = new Pokemon(pa10, a9, sa9, "Rossana", "16.07.2018", 112, 50, 1620, 33.44, 1.27, false);
		Pokemon p10 = new Pokemon(pa5, a10, sa10, "Hundemon", "21.08.2018", 134, 134, 2007, 38.23, 1.58, true);
		Pokemon p11 = new Pokemon(pa16, a5, sa11, "Pantimos", "18.08.2018", 87, 87, 1411, 81.92, 1.68, false);
		Pokemon p12 = new Pokemon(pa12, a11, sa12, "Aquana", "28.07.2016", 162, 162, 1545, 3.32, 1.01, true);
		Pokemon p13 = new Pokemon(pa6, a12, sa13, "Ursaring", "21.08.2018", 140, 140, 1828, 84.57, 1.64, false);
		Pokemon p14 = new Pokemon(pa7, a13, sa14, "Sengo", "30.07.2018", 125, 125, 1708, 49.69, 1.46, false);
		Pokemon p15 = new Pokemon(pa8, a14, sa15, "Dragonir", "16.09.2016", 74, 74, 578, 15.67, 3.89, false);
		Pokemon p16 = new Pokemon(pa9, a15, sa6, "Pinsir", "30.07.2018", 107, 107, 1704, 79.54, 1.7, true);

		// Bonbons erstellen
		Bonbon b1 = new Bonbon(65, pa1);
		Bonbon b2 = new Bonbon(63, pa2);
		Bonbon b3 = new Bonbon(16, pa3);
		Bonbon b4 = new Bonbon(86, pa4);
		Bonbon b5 = new Bonbon(95, pa5);
		Bonbon b6 = new Bonbon(36, pa6);
		Bonbon b7 = new Bonbon(76, pa7);
		Bonbon b8 = new Bonbon(88, pa8);
		Bonbon b9 = new Bonbon(94, pa9);
		Bonbon b10 = new Bonbon(10, pa10);
		Bonbon b11 = new Bonbon(102, pa11);
		Bonbon b12 = new Bonbon(80, pa12);
		Bonbon b13 = new Bonbon(75, pa13);
		Bonbon b14 = new Bonbon(54, pa14);
		Bonbon b15 = new Bonbon(79, pa15);
		Bonbon b16 = new Bonbon(68, pa16);

		
		
		
		
		
		
		
		
		
		
		// Spieler erstellen
		Spieler s1 = new Spieler("Jeldrik", 1000000, new Pokemon[6], new ArrayList<Pokemon>(), new ArrayList<Bonbon>());
		s1.getBonbons().addAll((Collection<? extends Bonbon>) Arrays.asList(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10,
				b11, b12, b13, b14, b15, b16));
		s1.getInBesitz().addAll((Collection<? extends Pokemon>) Arrays.asList(p1, p2, p3, p4, p5, p6, p7, p8));
		Spieler s2 = new Spieler("Pascal", 1000000, new Pokemon[6], new ArrayList<Pokemon>(), new ArrayList<Bonbon>());
		s2.getBonbons().addAll((Collection<? extends Bonbon>) Arrays.asList(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10,
				b11, b12, b13, b14, b15, b16));
		s2.getInBesitz().addAll((Collection<? extends Pokemon>) Arrays.asList(p9, p10, p11, p12, p13, p14, p15, p16));

		// Fenster Erstellen
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((int) screenSize.getWidth() / 2 - 200, (int) screenSize.getHeight() / 2 - 400, 400, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		CardLayout cl = new CardLayout();
		contentPane.setLayout(cl);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, "start");

		
		// Panel Spieler 1
		JLabel Spieler1 = new JLabel(new ImageIcon(ImageIO.read(getClass().getResource("/objekte/Hintergrund.png"))));
		tabbedPane.addTab(s1.getName(), null, Spieler1, null);
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		GridBagConstraints gbc2 = new GridBagConstraints();
		JLabel hintergrund = new JLabel(new ImageIcon(ImageIO.read(getClass().getResource("/objekte/Hintergrund2.png"))));
		hintergrund.setLayout(new GridLayout(0,2,0,0));
		hintergrund.setBounds(10, 0, 360, 735);
		Spieler1.add(hintergrund);
		JButton[] pokemon = new JButton[s1.getInBesitz().size()];
		JPanel[] base = new JPanel[s1.getInBesitz().size()];
		JLabel[] wp = new JLabel[s1.getInBesitz().size()];
		JLabel[] name = new JLabel[s1.getInBesitz().size()];
		JLabel[] bild = new JLabel[s1.getInBesitz().size()];
		JLabel[] space = new JLabel[s1.getInBesitz().size()];
		JProgressBar[] progressBar = new JProgressBar[s1.getInBesitz().size()];

		for (i = 0; i < s1.getInBesitz().size(); i++) {
			pokemon[i] = new JButton();
			pokemon[i].setBounds(0, 0, 220, 215);
			pokemon[i].setBorder(BorderFactory.createEmptyBorder());
			base[i] = new JPanel();
			base[i].setLayout(gbl);
			pokemon[i].setOpaque(false);
			pokemon[i].setContentAreaFilled(false);
			base[i].setOpaque(false);
			

			wp[i] = new JLabel("WP" + Integer.toString(s1.getInBesitz().get(i).getWp()));
			wp[i].setBounds(0, 0, wp[i].getPreferredSize().width, wp[i].getPreferredSize().height);
			name[i] = new JLabel(s1.getInBesitz().get(i).getName());
			name[i].setBounds(0, 0, name[i].getPreferredSize().width, name[i].getPreferredSize().height);
			double width = s1.getInBesitz().get(i).getPokemonArt().getBild().getWidth();
			double height = s1.getInBesitz().get(i).getPokemonArt().getBild().getHeight();
			bild[i] = new JLabel(new ImageIcon(s1.getInBesitz().get(i).getPokemonArt().getBild()
					.getScaledInstance((int) (100 * (width / height)), 100, Image.SCALE_SMOOTH)));
			bild[i].setBounds(0, 0, 110, 110);

			space[i] = new JLabel();

			progressBar[i] = new JProgressBar();
			progressBar[i].setForeground(Color.GREEN);
			progressBar[i].setValue(s1.getInBesitz().get(i).getAktKp() * 100 / s1.getInBesitz().get(i).getMaxKp());
			if (progressBar[i].getValue() < 50) {
				progressBar[i].setForeground(Color.YELLOW);
				if (progressBar[i].getValue() < 25) {
					progressBar[i].setForeground(Color.RED);
				}
			}
			
			bild[i].add(pokemon[i]);
			gbc.weighty = 0.1;
			hintergrund.add(base[i]);
			gbc.gridx = 1;
			gbc.gridy = 1;
			base[i].add(wp[i], gbc);
			gbc.gridx = 1;
			gbc.gridy = 2;
			base[i].add(bild[i], gbc);
			gbc.gridx = 1;
			gbc.gridy = 4;
			base[i].add(name[i], gbc);
			gbc2.gridx = 1;
			gbc2.gridy = 5;
			gbc2.ipady = -10;
			base[i].add(progressBar[i], gbc2);
			gbc.gridx = 1;
			gbc.gridy = 6;
			base[i].add(space[i], gbc);
			
		}
		// 9. Button
		pokemon[0].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s1, 0, cl);
			}
		});
		// 10. Button
		pokemon[1].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s1, 1, cl);
			}
		});
		// 11. Button
		pokemon[2].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s1, 2, cl);
			}
		});
		// 12. Button
		pokemon[3].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s1, 3, cl);
			}
		});
		// 13. Button
		pokemon[4].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s1, 4, cl);
			}
		});
		// 14. Button
		pokemon[5].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s1, 5, cl);
			}
		});
		// 15. Button
		pokemon[6].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s1, 6, cl);
			}
		});
		// 16. Button
		pokemon[7].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s1, 7, cl);
			}
		});

		// Panel Spieler 2
		JLabel Spieler2 = new JLabel(new ImageIcon(ImageIO.read(getClass().getResource("/objekte/Hintergrund.png"))));
		tabbedPane.addTab(s2.getName(), null, Spieler2, null);
		JButton[] pokemon2 = new JButton[s2.getInBesitz().size()];
		JPanel[] base2 = new JPanel[s2.getInBesitz().size()];
		JLabel[] wp2 = new JLabel[s2.getInBesitz().size()];
		JLabel[] name2 = new JLabel[s2.getInBesitz().size()];
		JLabel[] bild2 = new JLabel[s2.getInBesitz().size()];
		JLabel[] space2 = new JLabel[s2.getInBesitz().size()];
		JProgressBar[] progressBar2 = new JProgressBar[s2.getInBesitz().size()];
		JLabel hintergrund2 = new JLabel(new ImageIcon(ImageIO.read(getClass().getResource("/objekte/Hintergrund2.png"))));
		hintergrund2.setLayout(new GridLayout(0,2,0,0));
		hintergrund2.setBounds(10, 0, 360, 735);
		Spieler2.add(hintergrund2);
		

		for (i = 0; i < s2.getInBesitz().size(); i++) {
			pokemon2[i] = new JButton();
			pokemon2[i].setBounds(0, 0, 220, 215);
			pokemon2[i].setBorder(BorderFactory.createEmptyBorder());
			base2[i] = new JPanel();
			base2[i].setLayout(gbl);
			pokemon2[i].setOpaque(false);
			pokemon2[i].setContentAreaFilled(false);
			base2[i].setOpaque(false);

			wp2[i] = new JLabel("WP" + Integer.toString(s2.getInBesitz().get(i).getWp()));
			wp2[i].setBounds(0, 0, wp2[i].getPreferredSize().width, wp2[i].getPreferredSize().height);
			name2[i] = new JLabel(s2.getInBesitz().get(i).getName());
			name2[i].setBounds(0, 0, name2[i].getPreferredSize().width, name2[i].getPreferredSize().height);
			double width = s2.getInBesitz().get(i).getPokemonArt().getBild().getWidth();
			double height = s2.getInBesitz().get(i).getPokemonArt().getBild().getHeight();
			bild2[i] = new JLabel(new ImageIcon(s2.getInBesitz().get(i).getPokemonArt().getBild()
					.getScaledInstance((int) (100 * (width / height)), 100, Image.SCALE_SMOOTH)));
			bild2[i].setBounds(0, 0, 110, 110);

			space2[i] = new JLabel();

			progressBar2[i] = new JProgressBar();
			progressBar2[i].setForeground(Color.GREEN);
			progressBar2[i].setValue(s2.getInBesitz().get(i).getAktKp() * 100 / s2.getInBesitz().get(i).getMaxKp());
			if (progressBar2[i].getValue() < 50) {
				progressBar2[i].setForeground(Color.YELLOW);
				if (progressBar2[i].getValue() < 25) {
					progressBar2[i].setForeground(Color.RED);
				}
			}
			bild2[i].add(pokemon2[i]);
			gbc.weighty = 0.1;
		
			hintergrund2.add(base2[i]);
			gbc.gridx = 1;
			gbc.gridy = 1;
			base2[i].add(wp2[i], gbc);
			gbc.gridx = 1;
			gbc.gridy = 2;
			base2[i].add(bild2[i], gbc);
			gbc.gridx = 1;
			gbc.gridy = 4;
			base2[i].add(name2[i], gbc);
			gbc2.gridx = 1;
			gbc2.gridy = 5;
			gbc2.ipady = -10;
			base2[i].add(progressBar2[i], gbc2);
			gbc.gridx = 1;
			gbc.gridy = 6;
			base2[i].add(space2[i], gbc);
		}

		// 9. Button
		pokemon2[0].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s2, 0, cl);
			}
		});
		// 10. Button
		pokemon2[1].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s2, 1, cl);
			}
		});
		// 11. Button
		pokemon2[2].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s2, 2, cl);
			}
		});
		// 12. Button
		pokemon2[3].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s2, 3, cl);
			}
		});
		// 13. Button
		pokemon2[4].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s2, 4, cl);
			}
		});
		// 14. Button
		pokemon2[5].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s2, 5, cl);
			}
		});
		// 15. Button
		pokemon2[6].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s2, 6, cl);
			}
		});
		// 16. Button
		pokemon2[7].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onButtonClick(s2, 7, cl);
			}
		});
	
		
		
	}

	public void onButtonClick(Spieler s, int index, CardLayout cl) {
		PokemonAnsicht p1;
		try {
			p1 = new PokemonAnsicht(s, index, cl, contentPane);
			contentPane.add(p1, "p1");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		cl.show(contentPane, "p1");
	}
	
}