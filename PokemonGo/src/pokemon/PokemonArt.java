package pokemon;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class PokemonArt {

	private String artName;
	private String typ;
	private BufferedImage bild;
	public PokemonArt() {
		super();
	}
	public PokemonArt(String artName, String typ) throws IOException {
		super();
		this.artName = artName;
		this.typ = typ;
		this.bild = bekommeBild();
	}
	public String getArtName() {
		return artName;
	}
	public void setArtName(String artName) {
		this.artName = artName;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public BufferedImage getBild() {
		return bild;
	}
	public void setBild(BufferedImage bild) {
		this.bild = bild;
	}
	public ImageIcon bekommeHintergrund() throws IOException {
		ImageIcon hintergrund = new ImageIcon();
		if(getTyp().substring(0, 3).equals("Bod")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Boden.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Dra")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Drache.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Eis")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Eis.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Ele")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Elektro.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Fee")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Fee.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Feu")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Feuer.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Gei")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Geist.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Ges")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Gestein.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Gif")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Gift.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Käf")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Käfer.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Kam")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Kampf.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Luf")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Luft.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Nor")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Normal.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Pfl")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Pflanze.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Psy")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Psycho.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Sta")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Stahl.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Unl")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Unlicht.png"))));
		}
		else if(getTyp().substring(0, 3).equals("Was")) {
			hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Wasser.png"))));
		}
		else {
				hintergrund = new ImageIcon(ImageIO.read(getClass().getResource(("/hintergründe/Error.png"))));
			}
		return hintergrund;
		}
	public BufferedImage bekommeBild() throws IOException {
		BufferedImage bild = ImageIO.read(getClass().getResource("/pokemon/" + getArtName() + ".png"));
		return bild;
		
	}
		
	@Override
	public String toString() {
		return "PokemonArt [artName=" + artName + ", typ=" + typ + ", bild=" + bild + "]";
	}
}
