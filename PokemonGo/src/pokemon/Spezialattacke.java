package pokemon;

public class Spezialattacke extends Attacke{
	
	private int cooldown;

	public Spezialattacke() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Spezialattacke(String name, String typ, int schaden, int cooldown) {
		super(name, typ, schaden);
		// TODO Auto-generated constructor stub
		this.cooldown = cooldown;
	}

	public int getCooldown() {
		return cooldown;
	}

	public void setCooldown(int cooldown) {
		this.cooldown = cooldown;
	}

	@Override
	public String toString() {
		return "Spezialattacke [cooldown=" + cooldown + ", name=" + name + ", typ=" + typ + ", schaden=" + schaden
				+ "]";
	}
	
	

}
