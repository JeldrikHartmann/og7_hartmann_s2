package pokemon;


public class Pokemon {

	private PokemonArt pokemonArt;
	private Attacke standartAttacke;
	private Attacke spezialAttacke;
	private String name;
	private String einfangdatum;
	private int maxKp;
	private int aktKp;
	private int wp;
	private double gewicht;
	private double gr��e;
	private boolean favorit;

	public Pokemon() {
		super();
	}

	public Pokemon(PokemonArt pokemonArt, Attacke standartAttacke, Attacke spezialAttacke, String name, String einfangdatum, int maxKp,
			int aktKp, int wp, double gewicht, double gr��e, boolean favorit) {
		super();
		this.pokemonArt = pokemonArt;
		this.standartAttacke = standartAttacke;
		this.spezialAttacke = spezialAttacke;
		this.name = name;
		this.einfangdatum = einfangdatum;
		this.maxKp = maxKp;
		this.aktKp = aktKp;
		this.wp = wp;
		this.gewicht = gewicht;
		this.gr��e = gr��e;
		this.favorit = favorit;
	}

	public PokemonArt getPokemonArt() {
		return pokemonArt;
	}

	public void setPokemonArt(PokemonArt pokemonArt) {
		this.pokemonArt = pokemonArt;
	}

	public Attacke getStandartAttacke() {
		return standartAttacke;
	}

	public void setStandartAttacke(Attacke standartAttacke) {
		this.standartAttacke = standartAttacke;
	}

	public Attacke getSpezialAttacke() {
		return spezialAttacke;
	}

	public void setSpezialAttacke(Attacke spezialAttacke) {
		this.spezialAttacke = spezialAttacke;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEinfangdatum() {
		return einfangdatum;
	}

	public void setEinfangdatum(String einfangdatum) {
		this.einfangdatum = einfangdatum;
	}

	public int getMaxKp() {
		return maxKp;
	}

	public void setMaxKp(int maxKp) {
		this.maxKp = maxKp;
	}

	public int getAktKp() {
		return aktKp;
	}

	public void setAktKp(int aktKp) {
		this.aktKp = aktKp;
	}

	public int getWp() {
		return wp;
	}

	public void setWp(int wp) {
		this.wp = wp;
	}

	public double getGewicht() {
		return gewicht;
	}

	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}

	public double getGr��e() {
		return gr��e;
	}

	public void setGr��e(double gr��e) {
		this.gr��e = gr��e;
	}

	public boolean isFavorit() {
		return favorit;
	}

	public void setFavorit(boolean favorit) {
		this.favorit = favorit;
	}

	@Override
	public String toString() {
		return "Pokemon [pokemonArt=" + pokemonArt + ", standartAttacke=" + standartAttacke + ", spezialAttacke=" + spezialAttacke + ", name="
				+ name + ", einfangdatum=" + einfangdatum + ", maxKp=" + maxKp + ", aktKp=" + aktKp + ", wp=" + wp
				+ ", gewicht=" + gewicht + ", gr��e=" + gr��e + ", favorit=" + favorit + "]";
	}
}
