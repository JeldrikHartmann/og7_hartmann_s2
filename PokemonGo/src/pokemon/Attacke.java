package pokemon;

public class Attacke {

	protected String name;
	protected String typ;
	protected int schaden;
	public Attacke() {
		super();
	}
	public Attacke(String name, String typ, int schaden) {
		super();
		this.name = name;
		this.typ = typ;
		this.schaden = schaden;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public int getSchaden() {
		return schaden;
	}
	public void setSchaden(int schaden) {
		this.schaden = schaden;
	}
	@Override
	public String toString() {
		return "Attacke [name=" + name + ", typ=" + typ + ", schaden=" + schaden + "]";
	}
}
