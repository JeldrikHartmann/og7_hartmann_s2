package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;

public class BunteRechteckGUI extends JFrame {

	
	private static final long serialVersionUID = -5991643050005710417L;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItemNeuesRechteck;
	private BunteRechteckeController brc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		BunteRechteckGUI frame = new BunteRechteckGUI();
		frame.run();

	}

	protected void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.revalidate();
			this.repaint();
		}

	}

	/**
	 * Create the frame.
	 */
	public BunteRechteckGUI() {
		this.brc = new BunteRechteckeController();
		this.menuBar = new JMenuBar();
		this.menu = new JMenu("Datei");
		this.menuItemNeuesRechteck = new JMenuItem("Rechtecke verwalten");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		contentPane = new Zeichenflaeche(this.brc);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		setJMenuBar(menuBar);
		setVisible(true);
		this.menuBar.add(menu);
		menuItemNeuesRechteck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				menuItemNeuesRechteck_Clicked();
				
			}
			
		});

		this.menu.add(menuItemNeuesRechteck);
	}

	protected void menuItemNeuesRechteck_Clicked() {
		this.brc.rechteckHinzufügen();
		
	}

}
