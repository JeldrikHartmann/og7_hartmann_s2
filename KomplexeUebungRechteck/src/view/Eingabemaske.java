package view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

@SuppressWarnings("serial")
public class Eingabemaske extends JDialog {

	private JPanel contentPane;
	private JLabel höhe;
	private JLabel posX;
	private JLabel posY;
	private JLabel breite;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public Eingabemaske(BunteRechteckeController brc) {
		Timer timer = new Timer();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setTitle("Konfiguration");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(1, 0, 0, 0));
		setAlwaysOnTop(true);

		JSpinner spinX = new JSpinner();
		JSpinner spinY = new JSpinner();
		JSpinner spinWidth = new JSpinner();
		JSpinner spinHeight = new JSpinner();
		JLabel lblFehler = new JLabel("");
		JSpinner spinAnzahl = new JSpinner();

		JPanel panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		JButton fügeRechteckHinzu = new JButton("Rechteck Hinzuf\u00FCgen");
		panel.add(fügeRechteckHinzu);
		fügeRechteckHinzu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fügeRechteckHinzu(brc, lblFehler, spinX, spinY, spinWidth, spinHeight);
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						lblFehler.setForeground(Color.BLACK);
						lblFehler.setText("<html>Zu große Rechtecke<br>werden korrigiert!</html>");
					}
				}, 2000);
			}
		});

		lblFehler.setHorizontalAlignment(SwingConstants.CENTER);
		lblFehler.setText("<html>Zu große Rechtecke<br>werden korrigiert!</html>");
		panel.add(lblFehler);

		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(new GridLayout(1, 0, 0, 0));

		JButton ZufälligeRechtecke = new JButton("<html>Zufällige Rechtecke<br> Hinzuf\u00FCgen</html>");
		panel.add(ZufälligeRechtecke);
		panel_2.add(ZufälligeRechtecke);

		panel_2.add(spinAnzahl);

		JButton Zurücksetzen = new JButton("Zurücksetzen");
		Zurücksetzen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				brc.reset();
				
			}
		});
		panel.add(Zurücksetzen);

		JButton beenden = new JButton("Beenden");
		panel.add(beenden);
		beenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		ZufälligeRechtecke.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				new Eingabemaske(brc);
				brc.generiereZufallsRechtecke((int) spinAnzahl.getValue());
			}
		});

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 30));

		posX = new JLabel();
		posX.setHorizontalAlignment(SwingConstants.CENTER);
		posX.setText("<html>Position X<br>max. 1200</html>");
		panel_1.add(posX);

		spinX.setModel(new SpinnerNumberModel(0, 0, 1200, 1));
		panel_1.add(spinX);

		posY = new JLabel();
		posY.setHorizontalAlignment(SwingConstants.CENTER);
		posY.setText("<html>Position Y<br>max. 1000</html>");
		panel_1.add(posY);

		spinY.setModel(new SpinnerNumberModel(0, 0, 1000, 1));
		panel_1.add(spinY);

		breite = new JLabel();
		breite.setHorizontalAlignment(SwingConstants.CENTER);
		breite.setText("<html>Breite<br>max. 1200</html>");
		panel_1.add(breite);
		;

		spinWidth.setModel(new SpinnerNumberModel(0, 0, 1200, 1));
		panel_1.add(spinWidth);

		höhe = new JLabel();
		höhe.setHorizontalAlignment(SwingConstants.CENTER);
		höhe.setText("<html>H\u00F6he<br>max. 1000</html>");
		panel_1.add(höhe);

		spinHeight.setModel(new SpinnerNumberModel(0, 0, 1000, 1));

		panel_1.add(spinHeight);
		setVisible(true);

	}

	/**
	 * Create the frame.
	 */


	public void fügeRechteckHinzu(BunteRechteckeController brc, JLabel lblFehler, JSpinner spinX, JSpinner spinY,
			JSpinner spinWidth, JSpinner spinHeight) {
		try {
			spinX.commitEdit();
			spinY.commitEdit();
			spinWidth.commitEdit();
			spinHeight.commitEdit();
		} catch (java.text.ParseException e) {

		}
		int x = (int) spinX.getValue();
		int y = (int) spinY.getValue();
		int width = (int) spinWidth.getValue();
		int height = (int) spinHeight.getValue();
		((SpinnerNumberModel) spinWidth.getModel()).setMaximum(1200 - x);
		((SpinnerNumberModel) spinHeight.getModel()).setMaximum(1000 - y);
		if (!(width > (int) ((SpinnerNumberModel) spinWidth.getModel()).getMaximum())) {
			if (!(height > (int) ((SpinnerNumberModel) spinHeight.getModel()).getMaximum())) {
				lblFehler.setForeground(Color.GREEN);
				lblFehler.setText("Werte erfolgreich eingetragen");
			}
		}
		if (width > (int) ((SpinnerNumberModel) spinWidth.getModel()).getMaximum()) {
			width = (int) ((SpinnerNumberModel) spinWidth.getModel()).getMaximum();
			lblFehler.setForeground(Color.RED);
			lblFehler.setText("Korrigierte Werte eingetragen!");
		}
		if (height > (int) ((SpinnerNumberModel) spinHeight.getModel()).getMaximum()) {
			height = (int) ((SpinnerNumberModel) spinHeight.getModel()).getMaximum();
			lblFehler.setForeground(Color.RED);
			lblFehler.setText("Korrigierte Werte eingetragen!");
		}

		brc.add(new Rechteck(x, y, width, height));
		spinX.setValue(0);
		spinY.setValue(0);
		spinWidth.setValue(0);
		spinHeight.setValue(0);
		((SpinnerNumberModel) spinWidth.getModel()).setMaximum(1200);
		((SpinnerNumberModel) spinHeight.getModel()).setMaximum(1000);
	}
}
