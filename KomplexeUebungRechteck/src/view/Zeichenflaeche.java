package view;


import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;



import controller.BunteRechteckeController;

@SuppressWarnings("serial")
public class Zeichenflaeche extends JPanel {
	
	private BunteRechteckeController controllerobjekt;

	public Zeichenflaeche(BunteRechteckeController controllerobjekt) {
		this.controllerobjekt = controllerobjekt;
	}
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		for(int i = 0; i < controllerobjekt.getRechtecke().size(); i++)
		g.drawRect(controllerobjekt.getRechtecke().get(i).getX(), controllerobjekt.getRechtecke().get(i).getY(), controllerobjekt.getRechtecke().get(i).getBreite(), controllerobjekt.getRechtecke().get(i).getHoehe());
		
	}
}
