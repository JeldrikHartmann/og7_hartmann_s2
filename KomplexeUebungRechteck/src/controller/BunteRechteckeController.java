package controller;

import java.util.LinkedList;
import java.util.List;

import model.MySQLDatabase;
import model.Rechteck;
import view.Eingabemaske;

public class BunteRechteckeController {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	private List<Rechteck> rechtecke;
	private MySQLDatabase database;


	public BunteRechteckeController() {
		super();		
		this.rechtecke = new LinkedList<>();
		this.database = new MySQLDatabase();
		rechtecke = this.database.getAlleRechtecke();
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		this.database.rechteck_eintragen(rechteck);
	}

	public void reset() {
		rechtecke.clear();
	}

	public List<Rechteck> getRechtecke() {
		return rechtecke;
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		for(int i = 0; i < anzahl; i++) {
			rechtecke.add(Rechteck.generiereZufallsRechteck());
		}
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	public void rechteckHinzufügen() {
		new Eingabemaske(this);
		
		
	}

}
