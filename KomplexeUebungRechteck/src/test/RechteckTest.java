package test;

import java.util.ArrayList;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Rechteck r0 = new Rechteck();
		Rechteck r1 = new Rechteck();
		Rechteck r2 = new Rechteck();
		Rechteck r3 = new Rechteck();
		Rechteck r4 = new Rechteck();
		Rechteck r5 = new Rechteck(200, 200, 200, 200);
		Rechteck r6 = new Rechteck(800, 400, 20, 20);
		Rechteck r7 = new Rechteck(800, 450, 20, 20);
		Rechteck r8 = new Rechteck(850, 400, 20, 20);
		Rechteck r9 = new Rechteck(855, 455, 25, 25);
		r0.setX(10);
		r0.setY(10);
		r0.setBreite(30);
		r0.setHoehe(40);
		r1.setX(25);
		r1.setY(25);
		r1.setBreite(100);
		r1.setHoehe(20);
		r2.setX(260);
		r2.setY(10);
		r2.setBreite(200);
		r2.setHoehe(100);
		r3.setX(5);
		r3.setY(500);
		r3.setBreite(300);
		r3.setHoehe(25);
		r4.setX(100);
		r4.setY(100);
		r4.setBreite(100);
		r4.setHoehe(100);
		System.out.println(r0.toString());
		System.out.println(r1.toString());
		System.out.println(r2.toString());
		System.out.println(r3.toString());
		System.out.println(r4.toString());
		System.out.println(r5.toString());
		System.out.println(r6.toString());
		System.out.println(r7.toString());
		System.out.println(r8.toString());
		System.out.println(r9.toString());
		if (r0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"))
			System.out.println(true);
		else
			System.out.println(false);

		BunteRechteckeController b1 = new BunteRechteckeController();
		b1.add(r0);
		b1.add(r1);
		b1.add(r2);
		b1.add(r3);
		b1.add(r4);
		b1.add(r5);
		b1.add(r6);
		b1.add(r7);
		b1.add(r8);
		b1.add(r9);
		System.out.println(b1.toString());

		Rechteck r10 = new Rechteck(-4, -5, -50, -200);
		System.out.println(r10);

		Rechteck r11 = new Rechteck();
		r11.setX(-10);
		r11.setY(-10);
		r11.setBreite(-200);
		r11.setHoehe(-100);
		System.out.println(r11);
		

		System.out.println(rechteckeTesten());
	}
	
	public static boolean rechteckeTesten() {
		ArrayList<Rechteck> rlist=new ArrayList<Rechteck>();
		boolean on = false;
		Rechteck rtest = new Rechteck(0,0,1200,1000);
		for(int i = 0; i < 50000; i++) {
			rlist.add(Rechteck.generiereZufallsRechteck());
			if(rtest.enthaelt(rlist.get(i))) {
				on = true;
			}
			else 
				return false;				
		}
		
		return on;
		
	}
}
