package test;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;
import view.Zeichenflaeche;

@SuppressWarnings("serial")
public class RechteckViewTest extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		BunteRechteckeController brc = new BunteRechteckeController();
		new RechteckViewTest(brc);
		
		
		
	}

	

	/**
	 * Create the frame.
	 */
	public RechteckViewTest(BunteRechteckeController brc) {

		brc.add(new Rechteck(330, 330, 50, 50));
		brc.add(new Rechteck(380, 380, 50, 50));
		brc.add(new Rechteck(440, 440, 50, 50));
		brc.add(new Rechteck(500, 500, 50, 50));
		brc.add(new Rechteck(560, 440, 50, 50));
		brc.add(new Rechteck(620, 380, 50, 50));
		brc.add(new Rechteck(680, 330, 50, 50));
		brc.add(new Rechteck(740, 270, 50, 50));
		brc.add(new Rechteck(800, 210, 50, 50));
		brc.add(new Rechteck(860, 150, 50, 50));

		setTitle("RechteckViewTest");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1260, 1080);
		contentPane = new Zeichenflaeche(brc);
		Graphics g = null;
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.paintComponents(g);
		setContentPane(contentPane);
		this.setVisible(true);
	}

}
