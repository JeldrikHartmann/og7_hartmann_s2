package model;

import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import java.sql.Statement;

public class MySQLDatabase {

	String driver = "com.mysql.jdbc.Driver";
	String url = "jdbc:mysql://localhost/rechtecke";
	String user = "root";
	String password = "";

	public void rechteck_eintragen(Rechteck r) {
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_rechtecke (x,y,breite,hoehe) values(?,?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			ps.executeUpdate();
			con.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Rechteck> getAlleRechtecke() {
		List<Rechteck> rechtecke = new LinkedList<Rechteck>();
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_rechtecke;";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			// Da nach con.close() rs zur�ckgesetzt wird m�ssen wir die daten anders
			// auslesen:
			// (next(), solange daten Vorhanden)
			while (rs.next()) {
				int x = rs.getInt("x");
				int y = rs.getInt("y");
				int breite = rs.getInt("breite");
				int hoehe = rs.getInt("hoehe");
				rechtecke.add(new Rechteck(x, y, breite, hoehe));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rechtecke;
	}
}
