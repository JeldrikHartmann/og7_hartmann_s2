package model;

public class Rechteck {

	private Punkt p = new Punkt(0, 0);
	private int breite;
	private int hoehe;

	public Rechteck() {
		super();
		this.p.setX(0);
		this.p.setY(0);
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.p.setX(x);
		this.p.setY(y);
		setBreite(breite);
		setHoehe(hoehe);
	}

	public int getX() {
		return this.p.getX();
	}

	public void setX(int x) {
		this.p.setX(x);
	}

	public int getY() {
		return this.p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}

	public boolean enthaelt(int x, int y) {
		boolean istIn = false;
		int rechteKante = getX() + getBreite();
		int untereKante = getY() + getHoehe();
		if (x >= getX() && x <= rechteKante && y >= getY() && y <= untereKante)
			istIn = true;
		else
			istIn = false;

		return istIn;

	}

	public boolean enthaelt(Punkt p) {
		int x = p.getX();
		int y = p.getY();
		return enthaelt(x, y);
	}

	public boolean enthaelt(Rechteck rechteck) {
		boolean on = false;
		int rechteckBreite = rechteck.getX()+rechteck.getBreite();
		int rechteckHoehe = rechteck.getY()+rechteck.getHoehe();
		if(this.enthaelt(rechteck.getX(),rechteck.getY())) {
		    if(this.enthaelt(rechteckBreite, rechteckHoehe)){
		        on = true;
		    }
		}
		return on;
		}
	
	public static Rechteck generiereZufallsRechteck() {
		int x = (int) (Math.random() * 1200);
		int y = (int)(Math.random() * 1000);
		int breite = (int)(Math.random() * (1200 - x));
		int hoehe = (int)(Math.random() * (1000 - y));
		Rechteck r1 = new Rechteck(x, y, breite, hoehe);
		return r1;
	}
	
	@Override
	public String toString() {
		return "Rechteck [p=" + p + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}

}
